﻿package pathfinding {
	
	public interface PathFindable {

		// Interface methods:
		
		// return null if no walkable tile there
		function getTile(inputX:int, inputY:int):Tile;
	}
	
}
