﻿/*
this is used in the A* to figure out a path
each node contains F,G,H cost:

G = the movement cost to move from the starting point A to a given square on the grid, 
    following the path generated to get there.  
H = the estimated movement cost to move from that given square on the grid to the final destination, point B. 
F = G + H
*/

package pathfinding {
	
	import pathfinding.Tile;
	
	public class PathNode extends Tile {
		
		public static const STRAIGHT_MOVEMENT_COST:int = 10;
		public static const DIAGONAL_MOVEMENT_COST:int = 14;
		
		public var parent:PathNode;
		
		public var gCost:int; 
		public var fCost:int; 
		public var hCost:int;
		
		public function PathNode(inputTarget:Tile, inputX:int, inputY:int, startingGCost:int = 0):void {
			super(inputX, inputY); // walkable is ignored, since it must be part of a path (hence defaulting to true)
			
			this.gCost = startingGCost;
			this.hCost = calculateHCost(inputTarget);
			updateFCost();
			
			parent = null; // to start
		}
		
		public function updateFCost():void {
			this.fCost = this.gCost + this.hCost;
		}
		
		private function calculateHCost(inputTarget:Tile):int {
			return (Math.abs(inputTarget.x - this.x) + Math.abs(inputTarget.y - this.y)) * STRAIGHT_MOVEMENT_COST;
		}
		
		// Diagonal heuristic - seems to give the same result? - can test this later
		/*private function calculateHCost(inputTarget:Tile):int {
			var xDiff:int = Math.abs(inputTarget.x - this.x);
			var yDiff:int = Math.abs(inputTarget.y - this.y);
			
			var diagonal:int = Math.min(xDiff, yDiff);
			var straight:int = xDiff + yDiff;
			
			var result:int = (diagonal * DIAGONAL_MOVEMENT_COST) + (STRAIGHT_MOVEMENT_COST * (straight - 2 * diagonal));
			return result;
		}*/
		
		
	}
	
	
	
}









