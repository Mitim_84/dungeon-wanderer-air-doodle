﻿package pathfinding {
	
	public class Tile {
		
		public var walkable:Boolean;
		
		public var x:int;
		public var y:int;
		
		public function Tile(inputX:int, inputY:int, inputWalkable:Boolean = true):void {
			this.walkable = inputWalkable;
			this.x = inputX;
			this.y = inputY;
		}
		
		public function toString():String {
			return "Tile - (" + this.x +", " + this.y +")";
			// use this for an ascii representation (if printed out of a 2d array or something)
			//return (this._walkable == true) ? (".") : ("@");
		}
	}
	
}
