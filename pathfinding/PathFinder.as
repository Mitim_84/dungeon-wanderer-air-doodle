﻿package pathfinding {
	
	import pathfinding.Tile;
	import pathfinding.PathFindable;
	
	import pathfinding.PathNode;
	import flash.geom.Point;
	
	public class PathFinder {
		
		// these numbers are mostly arbitrary
		// only that 'avoid weight' needs to be positive and 'attract weight' needs to be negative
		// at the moment, '5' should mean this path is 5 times less desirable then the neighbouring paths
		public static const AVOID_WEIGHT:int = 5;
		public static const ATTRACT_WEIGHT:int = -5;
		
		private var world:PathFindable;
		public var _allowDiagonals:Boolean;
		
		private var openList:Vector.<PathNode>;
		
		// changed this in to a dictionary, since we don't need to care about any sorted order or not,
		// just need to check if something is in here or not
		//
		// the dictionary key is the node's x and y added together in a string with a comma, for example: "6,7"
		// (had massive improvement processing gains here)
		//private var closedList:Vector.<PathNode>;
		private var closedList:Object;
		
		private var destinationTile:Tile;
		private var originTile:PathNode;
		
		// this holds a bunch of indexes to look up adjacent nodes
		// holds -1, 0, x, 1, 0, x, 0, -1, x, 0, 1, x
		// every 3 numbers are used to add to x and y of a sqaure to get the index of an adjacent square as well as the cost to move there
		// example: first 3 numbers are -1, 0, and x, so to move to the first adjacent square of 'n', go:
		// (n.x + -1) , (n.y + 0) and the travel cost would be 'x'
		private var adjacentOffsets:Vector.<int>;
		//
		// same as above, except for diagonals
		// holds -1, -1, x, 1, -1, x, -1, 1, x, 1, 1, x
		private var diagonalOffsets:Vector.<int>;
		
		// the actual offsets the pathfinder will use to find surrounding tiles
		// created from "adjacentOffsets" and "diagonalOffsets" as needed
		private var usingOffsets:Vector.<int>;
		
		// this is a dictionary object that holds extra weight adjustments for tiles if needed
		// extra weight or less weight can be added to a tile to make the path finding algorithm
		// favor or discourage certain tiles
		// (dictionary key is the same as closedList)
		private var weightAdjustments:Object;
		
		public function PathFinder(inputAllowDiagonals:Boolean = true):void {
			
			adjacentOffsets = new <int>[-1, 0, PathNode.STRAIGHT_MOVEMENT_COST, 
										1, 0, PathNode.STRAIGHT_MOVEMENT_COST,
										0, -1, PathNode.STRAIGHT_MOVEMENT_COST,
										0, 1, PathNode.STRAIGHT_MOVEMENT_COST];
										
			diagonalOffsets = new <int>[-1, -1, PathNode.DIAGONAL_MOVEMENT_COST,
										1, -1, PathNode.DIAGONAL_MOVEMENT_COST,
										-1, 1, PathNode.DIAGONAL_MOVEMENT_COST,
										1, 1, PathNode.DIAGONAL_MOVEMENT_COST];
			
			this.allowDiagonals = inputAllowDiagonals;
			
			clearWeightAdjustments();
		}
		
		public function set allowDiagonals(inputBoolean:Boolean):void {
			
			this._allowDiagonals = inputBoolean;
			
			if(this._allowDiagonals == true){
				usingOffsets = adjacentOffsets.concat(diagonalOffsets);
			} else {
				usingOffsets = adjacentOffsets;
			}
		}
		
		public function get allowDiagonals():Boolean {			return this._allowDiagonals;		}
		
		public function clearWeightAdjustments():void {
			weightAdjustments = new Object();
		}
		
		
		public function findPath(inputWorld:PathFindable, inputStart:Tile, inputEnd:Tile):Vector.<Tile> {
			world = inputWorld;
			
			destinationTile = inputEnd;
			originTile = new PathNode(destinationTile, inputStart.x, inputStart.y, 0);
			
			openList = new Vector.<PathNode>();
			//closedList = new Vector.<PathNode>();
			closedList = new Object();
			//var visitedNodes:int = 0;
			
			openList.push(originTile);
			
			
			var currentTile:PathNode;
			while(openList.length > 0){
				
				currentTile = openList.shift();
				
				if((currentTile.x == destinationTile.x) && (currentTile.y == destinationTile.y)){
					//trace("Path found: ");
					//printPath(currentTile);
					//trace("Number of nodes in open list: "+ openList.length);
					//trace("Number of nodes in closed list: "+ visitedNodes);
					return buildAndReturnPath(currentTile);
				}
				
				//closedList.push(currentTile);
				closedList[currentTile.x +","+ currentTile.y] = currentTile;
				//visitedNodes++;
				
				searchSurrondingTiles(currentTile);
				
			} // end of while loop
			
			//trace("Number of nodes in closed list: "+ visitedNodes);
			//trace("No path found");
			return null;
		}
		
		
		private function sortByFCost(inputA:PathNode, inputB:PathNode):int {
			if(inputA.fCost < inputB.fCost){		return -1;		}
			else if(inputA.fCost > inputB.fCost){	return 1;		}
			else {			return 0;			}
		}
		
		private function existsInList(inputList:Vector.<PathNode>, inputX:int, inputY:int):int {
			for(var counter:int = 0; counter < inputList.length; counter++){
				if((inputList[counter].x == inputX) && (inputList[counter].y == inputY)){
					return counter;
				}
			}
			return -1;
		}
		
		// returns true or not if the target tile is reached
		private function searchSurrondingTiles(inputTile:PathNode):void {
			
			var counter:int = 0;
			var limit:int = usingOffsets.length;
			var tempTile:Tile;
			var tempPathNode:PathNode;
			var tempIndex:int;
			
			for(counter = 0; counter < limit; counter += 3){
				tempTile = world.getTile(inputTile.x + usingOffsets[counter], 
										 inputTile.y + usingOffsets[counter + 1]);
				
				/*trace(counter);
				trace("Using offset: "+ adjacentOffsets[counter], adjacentOffsets[counter + 1]);
				trace("\tResulting tile:"+ tempTile);*/
				
				if(tempTile != null){
					
					if(closedList[tempTile.x +","+ tempTile.y] != null){
						continue;
					}
					
					tempPathNode = new PathNode(destinationTile, tempTile.x, tempTile.y, 
													inputTile.gCost + usingOffsets[counter + 2] +
													getWeightAdjustment(tempTile.x, tempTile.y));
					
					tempPathNode.parent = inputTile;
					tempIndex = existsInList(openList, tempPathNode.x, tempPathNode.y);
					
					if(tempIndex != -1){
						resolveOpenListCollision(tempPathNode, openList[tempIndex]);
					} else {
						openList.push(tempPathNode);
					}
				}
			}
		} // end of searchSurrondingTiles() method
		
		
		// handles the case if a tile is already on the open list
		private function resolveOpenListCollision(currentTile:PathNode, existingOpenTile:PathNode):void {
			
			if((currentTile.gCost) < existingOpenTile.gCost){
				existingOpenTile.parent = currentTile;
				existingOpenTile.gCost = currentTile.gCost;
				existingOpenTile.updateFCost();
				
				openList.sort(sortByFCost);
			}
		}
		
		private function buildAndReturnPath(inputPathNode:PathNode):Vector.<Tile> {
			var result:Vector.<Tile> = new Vector.<Tile>();
			var currentNode:PathNode = inputPathNode;
			
			while(currentNode.parent != null){
				result.unshift(new Tile(currentNode.x, currentNode.y));
				currentNode = currentNode.parent;
			}
			// add the final node to the front of the path (since it would be the first node moving backwards)
			result.unshift(new Tile(currentNode.x, currentNode.y));
			return result;
		}
		
		private function printPath(inputPathNode:PathNode):void {
			var currentNode:PathNode = inputPathNode;
			trace("Start of path:");
			
			while(currentNode.parent != null){
				trace("\t(" + currentNode.x +", "+ currentNode.y +")");
				currentNode = currentNode.parent;
			}
			// trace the final node to the front of the path (since it would be the first node moving backwards)
			trace("\t(" + currentNode.x +", "+ currentNode.y +")");
		}
		
		
		public function setWeightAdjustment(inputX:int, inputY:int, inputValue:int):void {
			weightAdjustments[inputX +","+ inputY] = inputValue;
		}
		
		private function getWeightAdjustment(inputX:int, inputY:int):int {
			return (weightAdjustments[inputX +","+ inputY] != null) ? (weightAdjustments[inputX +","+ inputY]) : 0;
		}
		
	} // end of class
} // end of package
