﻿// this is a wrapper around a BitmapData object that contains a bitmap representing a world
// (this is mostly just for quick pathfinder testing and benchmarking)
package dungeon {
	
	
	import pathfinding.Tile;
	import pathfinding.PathFindable;
	import flash.display.BitmapData;
	
	public class BitmapWorld implements PathFindable {
		
		private static var WALKABLE:uint = 0xffffff;
		private static var UNWALKABLE:uint = 0x000000;
		
		private var bitmapData:BitmapData;
		
		public function BitmapWorld(inputData:BitmapData):void {
			bitmapData = inputData;
		}
		
		public function getTile(inputX:int, inputY:int):Tile {
			
			if((inputX < 0) || (inputX >= bitmapData.width)){
				return null;
			} else if((inputY < 0) || (inputY >= bitmapData.height)){
				return null;
			} else {
				var colour:uint = bitmapData.getPixel(inputX, inputY);
				if(colour != WALKABLE){
					return null;
				} else {
					return new Tile(inputX, inputY);
				}
			}
		} // end of getTile()
		
		
		public function getRandomTile():Tile {
			var result:Tile = new Tile(Math.floor(Math.random() * bitmapData.width), Math.floor(Math.random() * bitmapData.height));
			//trace("getting another tile");
			while(bitmapData.getPixel(result.x, result.y) == UNWALKABLE){
				
				result.x = Math.floor(Math.random() * bitmapData.width);
				result.y = Math.floor(Math.random() * bitmapData.height);
				
				//trace("\tLooking for random tile:", "Checking "+ result.toString(),
				//bitmapData.getPixel(result.x, result.y), "vs", UNWALKABLE, ":", bitmapData.getPixel(result.x, result.y) == UNWALKABLE);
			}
			
//			trace("result", bitmapData.getPixel(result.x, result.y), UNWALKABLE, WALKABLE);
			//trace(bitmapData.getPixel(result.x, result.y) == UNWALKABLE);
			return result;
		}
		
	} // end of class
} // end of package










