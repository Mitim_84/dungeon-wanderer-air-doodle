﻿// this is a little creature that wanders the dungeon
// it starts at a spot and is given a path to another spot
// when it finishes following that path, it waits for a new one
package dungeon {
	
	import pathfinding.Tile;
	
	import flash.geom.Point;
	import flash.events.EventDispatcher;
	import flash.events.Event;
	
	public class Wanderer extends EventDispatcher {
		
		private const speed:int = 2;
		private var currentPos:Point;
		private var movementVector:Point;
		
		private var currentPath:Vector.<Tile>;
		private var currentStep:int;
		
		private var facingDirection:Point;
		
		private var _id:int;
		
		public var lastStep:Tile;
		
		public function Wanderer(inputId:int, startingX:int, startingY:int):void {
			currentPos = new Point(startingX, startingY);
			movementVector = new Point();
			facingDirection = new Point();
			this._id = inputId;
		}
		
		public function setPath(inputPath:Vector.<Tile>):void {
			if(inputPath == null){
				trace("Wanderer.setPath(): null path given");
			}
			currentPath = inputPath;
			currentStep = 0;
			figureOutFinalDirection();			
		}
		
		
		public function getPos():Point {		return this.currentPos;			}
		
		// just for convenience
		public function get x():int {			return this.currentPos.x;		}
		public function get y():int {			return this.currentPos.y;		}
		
		public function get id():int {			return this._id;				}
		public function getFacing():Point {		return this.facingDirection;	}
		
		// look at the last two points in the path to figure out 
		// the final facing direction when the sprite is done the current path
		private function figureOutFinalDirection():void {
			if(currentPath == null){
				trace("Wanderer.figureOutFinalDirection() - Can't figure out final direction: null path error");
				facingDirection = new Point();
				// dispatch this to prompt for a new path
				dispatchEvent(new Event(Event.COMPLETE));
				return;
			}
			
			if(currentPath.length > 2){
				var lastPoint:Tile = currentPath[currentPath.length - 1];
				var secondLastPoint:Tile = currentPath[currentPath.length - 2];
				facingDirection = new Point(lastPoint.x - secondLastPoint.x, lastPoint.y - secondLastPoint.y);
				facingDirection.normalize(1);
			}
		} // end of figureOutFinalDirection() method
		
		// check if reached the current step
		// if so, move to next step
		// if not, then calculate distace left and move that amount limited by speed if needed
		public function step():void {
			
			if(currentPath == null){	return;			}
			
			// done following the current path
			if(currentStep == currentPath.length){
				dispatchEvent(new Event(Event.COMPLETE));
				return;
			}
			
			if((this.currentPos.x == currentPath[currentStep].x) && (this.currentPos.y == currentPath[currentStep].y)){
				currentStep++;
				
			} else {
				movementVector.x = currentPath[currentStep].x - this.currentPos.x;
				movementVector.y = currentPath[currentStep].y - this.currentPos.y;
				
				if(movementVector.length > speed){
					movementVector.normalize(speed);
				}
				
				this.currentPos.x += movementVector.x;
				this.currentPos.y += movementVector.y;
			}
		} // end of step() method
		
	} // end of Wanderer{} class
} // end of package
