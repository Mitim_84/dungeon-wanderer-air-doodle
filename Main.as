package {
	import dungeon.BitmapWorld;
	import dungeon.Wanderer;
	
	import pathfinding.PathFinder;
	import pathfinding.Tile;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import flash.utils.getTimer;
	
	import flash.net.URLRequest;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Loader;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	
	import flash.net.URLRequest;
	
	import flash.geom.Point;
	
	
	public class Main extends Sprite {
		
		private const mapBitmapString:String = "cleaned map bitmap.png";
		private const WANDERER_AMOUNT:int = 100;
		
		private var displayScale:Number = 1;
		
		private var pathFinder:PathFinder;
		private var bitmapWorld:BitmapWorld;
		
		private var fpsDisplay:TextField;
		private var msDisplay:TextField;
		
		private const PATHING_PROCESS_LIMIT:int = 5;
		// this is a queue that stores the ids of wanderers that are waiting for a new path
		// only a certain amount will be processed per frame tick to spread out the processing load
		private var pathingQueue:Vector.<int>;
		
		private var wanderers:Vector.<Wanderer>;
		private var wandererSprites:Vector.<Sprite>;
		
		
		// just some performance info stuff here
		private var frameStartTime:int = 0;
		private var frameEndTime:int = 0;
		private var secondTimeCount:int = 0;
		private var tickCount:int = 0;

		
		public function Main():void {
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, deactivate);
			stage.addEventListener(Event.ACTIVATE, activate);
			
			secondTimeCount = getTimer();
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			// entry point
			
			pathFinder = new PathFinder(false);
			var rawBitmapMap:Loader = new Loader();
			rawBitmapMap.contentLoaderInfo.addEventListener(Event.COMPLETE, loadedBitmap, false, 0, true);
			
			// add two textfield displays, one for frame time and the other for fps
			var textFormat:TextFormat = new TextFormat("Verdana", 15, 0x00ff00);
			textFormat.align = "right";
			
			fpsDisplay = new TextField();
			msDisplay = new TextField();
			fpsDisplay.defaultTextFormat = msDisplay.defaultTextFormat = textFormat;
			fpsDisplay.selectable = msDisplay.selectable = false;
			//fpsDisplay.border = true;
			fpsDisplay.width = msDisplay.width = 100;
			fpsDisplay.height = msDisplay.height = 20;
			fpsDisplay.x = msDisplay.x = 350;
			fpsDisplay.y = 490;
			msDisplay.y = 510;
			
			fpsDisplay.text = "-1 FPS";
			msDisplay.text = "-1 ms";
			
			// add these when the bitmap is done loading, as they may get covered up
			//this.addChild(fpsDisplay);
			//this.addChild(msDisplay);
			
			pathingQueue = new Vector.<int>();
			
			wanderers = new Vector.<Wanderer>();
			wandererSprites = new Vector.<Sprite>();
			
			// finally, start by loading in the bitmap which represents the dungeon
			rawBitmapMap.load(new URLRequest(mapBitmapString));
		}
		
		
		private function loadedBitmap(inputEvent:Event):void {
			
			var rawBitmapMap:Loader = ((Loader) (inputEvent.currentTarget.loader));
			var worldBitmap:BitmapData = new BitmapData(rawBitmapMap.width, rawBitmapMap.height, false, 0xff0000);
			worldBitmap.draw(rawBitmapMap);
			
			// adjust the display scale to fit the bitmap to the stage
			this.displayScale = this.stage.stageWidth / rawBitmapMap.width;
			
			// just scale up the pixel bitmap for drawing
			var mapDisplay:Bitmap = new Bitmap(worldBitmap);
			mapDisplay.scaleX = mapDisplay.scaleY = this.displayScale;
			this.addChild(mapDisplay);
			
			// create the world (for path finding)
			bitmapWorld = new BitmapWorld(worldBitmap);
			
			for(var counter:int = 0; counter < WANDERER_AMOUNT; counter++){
				addWanderer(this);
			}
			
			// add these here (so they'll appear on top)
			this.addChild(fpsDisplay);
			this.addChild(msDisplay);
			
			// moved to activate() method
			//this.addEventListener(Event.ENTER_FRAME, frameTick, false, 0, true);
		}
		
		
		private function deactivate(e:Event):void {
			// make sure the app behaves well (or exits) when in background
			//NativeApplication.nativeApplication.exit();
			this.removeEventListener(Event.ENTER_FRAME, frameTick, false);
		}
		
		private function activate(e:Event):void {
			this.addEventListener(Event.ENTER_FRAME, frameTick, false, 0, true);
		}
		
		
		private function addWanderer(inputParent:DisplayObjectContainer):void {
			var startingPos:Tile = bitmapWorld.getRandomTile();
			
			var temp:Tile = new Tile(startingPos.x, startingPos.y);
			
			// scale this up, since the tiles are only pixel/tile coords
			startingPos.x *= this.displayScale;
			startingPos.y *= this.displayScale;
			
			var wanderer:Wanderer = new Wanderer(wanderers.length, startingPos.x, startingPos.y);
			
			// a workaround for a rounding bug: store the original non-scaled up last step of the found path
			// it will be used as the starting point to calculate the wanderer's next path
			wanderer.lastStep = temp;
			
			
			var wandererSprite:Sprite = new Sprite();
			wandererSprite.graphics.beginFill(randomColour());
			wandererSprite.graphics.drawRect(0, 0, this.displayScale, this.displayScale);
			wandererSprite.graphics.endFill();
			wandererSprite.cacheAsBitmap = true;
			
			wandererSprite.x = wanderer.x;
			wandererSprite.y = wanderer.y;
			
			wanderer.addEventListener(Event.COMPLETE, wandererArrived, false, 0, true);
			
			// instead of calculating a new path right away, push to a queue to avoid having to calculate many paths all at once
			//setNewPath(wanderer);
			pathingQueue.push(wanderer.id);
			
			wanderers.push(wanderer);
			wandererSprites.push(wandererSprite);
			
			inputParent.addChild(wandererSprite);
		} // end of addWanderer() method
		
		
		private function setNewPath(inputWanderer:Wanderer):void {
			
			// instead of trying to calculate the position of the wanderer back to pixel/tile coords (which led to rounding issues)
			// just use the stored 'lastStep' position, which should be the last step of the wanderer's finished path
			//var startTile:Tile = new Tile(Math.floor(inputWanderer.x / this.displayScale), Math.floor(inputWanderer.y / this.displayScale));
			var startTile:Tile = inputWanderer.lastStep;
			
			var endTile:Tile = bitmapWorld.getRandomTile();
			// update this here, since the last step is known now for the wanderer's new path
			inputWanderer.lastStep = endTile;
			
			
			var facingDirection:Point = inputWanderer.getFacing();
			
			pathFinder.clearWeightAdjustments();
			pathFinder.setWeightAdjustment(startTile.x - facingDirection.x, startTile.y - facingDirection.y, PathFinder.AVOID_WEIGHT);
			
			var foundPath:Vector.<Tile> = pathFinder.findPath(bitmapWorld, startTile, endTile);
			
			if(foundPath == null){
				trace("Issue: Path not found here:", startTile, endTile);
			}
			
			// need to scale up the path for smoother animated motion, else it could only move 1 'block/pixel' at a time
			// note that this means wanderer sprites need to be drawn separately and should not be scaled up using "this.displayScale"
			// as it's positions are already scaled up here
			for(var step:String in foundPath){
				foundPath[step].x *= this.displayScale;
				foundPath[step].y *= this.displayScale;
			}
			
			inputWanderer.setPath(foundPath);
		} // end of setNewPath() method
		
		 // try to return a number between: 0x222222 and 0xcccccc
		// (just guessing on how the math works here), doesn't need to be perfect
		private function randomColour():uint {
			return ((0xffffff - 0x444444) * Math.random()) + 0x222222;
		}
		
		private function wandererArrived(inputEvent:Event):void {
			//setNewPath((Wanderer) (inputEvent.target));
			pathingQueue.push(((Wanderer) (inputEvent.target)).id);
		}
		
		private function processPathingQueue():void {
			var limit:int = Math.min(PATHING_PROCESS_LIMIT, pathingQueue.length);
			for(var counter:int = 0; counter < limit; counter++){
				setNewPath(wanderers[pathingQueue.pop()]);
			}
		}
		
		
		// for each frame tick, step and update the movement of the corresponding sprite
		private function frameTick(inputEvent:Event):void {
			
			
			frameStartTime = getTimer();
			
			// process this if needed
			if(pathingQueue.length > 0){
				processPathingQueue();
			}
			
			
			for(var wanderer:String in wanderers){
				wanderers[wanderer].step();
				wandererSprites[wanderer].x = wanderers[wanderer].x;
				wandererSprites[wanderer].y = wanderers[wanderer].y;
			}
			
			
			// just some performance info stuff here (calculate and display fps and frame time)
			tickCount++;
			
			frameEndTime = getTimer();
			
			if((frameEndTime - secondTimeCount) >= 1000){
				secondTimeCount = frameEndTime;
				fpsDisplay.text = tickCount +" FPS";
				tickCount = 0;
			}
			
			msDisplay.text = (frameEndTime - frameStartTime) +"ms";
			
		} // end of frameTick() method
		
	} // end of class
} // end of package